﻿export default interface Address {
    country: string,
    street: string,
    number: string,
    code: string
}