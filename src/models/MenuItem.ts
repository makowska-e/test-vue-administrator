﻿export default interface MenuItem {
    label: String,
    icon: String,
    to: String,
}