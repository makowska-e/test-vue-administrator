﻿import Address from "./Address"

export default interface NewUser {
    name: string,
    surname: string,
    email: string,
    company: string,
    address: {
        country: string,
        street: string,
        number: string,
        code: string
    }
}