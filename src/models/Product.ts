﻿export default interface Address {
    id: Number,
    name: string,
    description: string,
    price: Number
}