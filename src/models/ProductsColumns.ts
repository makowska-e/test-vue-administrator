﻿interface Header {
    text: String,
    align: string,
    sortable: Boolean,
    value: String
}

export default interface ProductsColumn {
    header: Array<Header>,
    itemsPerPage: Number,
}