﻿import Address from "./Address"

export default interface Users {
    id: Number,
    name: string,
    surname: string,
    email: string,
    company: string,
    address: Address
}