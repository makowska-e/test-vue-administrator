﻿interface Header {
    text: String,
    align: string,
    sortable: Boolean,
    value: String
}

export default interface UsersColumns {
    header: Array<Header>,
    itemsPerPage: Number,
}