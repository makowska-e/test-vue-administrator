import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'

import Panel from '../components/panel/Panel.vue';
import UsersList from '../components/users/List.vue'
import UserAdd from '../components/users/Add.vue'
import UserEdit from '../components/users/Edit.vue'
import ProductsList from '../components/products/List.vue'
import ProductAdd from '../components/products/Add.vue'
import ProductEdit from '../components/products/Edit.vue'

Vue.use(VueRouter)

  const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'Panel',
    component: Panel,
    props: true
  },
  {
    path: '/users/list',
    name: 'UsersList',
    component: UsersList,
    props: true
  },
  {
    path: '/users/add',
    name: 'UsersAdd',
    component: UserAdd,
    props: true
  },
  {
    path: '/users/edit/:id',
    name: 'UserEdit',
    component: UserEdit,
    props: true
  },
  {
    path: '/products/list',
    name: 'ProductsList',
    component: ProductsList,
    props: true
  },
  {
    path: '/products/add',
    name: 'ProductstAdd',
    component: ProductAdd,
    props: true
  },
  {
    path: '/products/edit',
    name: 'ProductEdit',
    component: ProductEdit,
    props: true
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
